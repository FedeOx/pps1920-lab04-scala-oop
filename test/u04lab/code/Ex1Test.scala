package u04lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u04lab.code._

class Ex1Test {

  @Test def testComplex(): Unit = {
    val a = Array(Complex(10,20), Complex(1,1), Complex(7,0))
    val c = a(0) + a(1) + a(2)
    assertEquals(c, ComplexImpl(18.0,21.0))
    val c2 = a(0) * a(1)
    assertEquals(c2, ComplexImpl(-10.0,30.0))
  }

}
