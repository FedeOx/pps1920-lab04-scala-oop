package u04lab.code

import Optionals._
import Lists._

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

case class IncrementalPowerIterator[A](val start: Int, val successive: Int => Int) extends PowerIterator[A] {

  override def next(): Option[A] = ???

  override def allSoFar(): List[A] = ???

  override def reversed(): PowerIterator[A] = ???
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = IncrementalPowerIterator(start, successive)

  override def fromList[A](list: List[A]): PowerIterator[A] = ???

  override def randomBooleans(size: Int): PowerIterator[Boolean] = ???
}
